import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

const ELEMENT_DATA = [
  {position:  'Sys 1', name: 'documents 1', weight: 1.0079, symbol: 'H'},
  {position:  'Sys 2', name: 'documents 2', weight: 4.0026, symbol: 'He'},
  {position:  'Sys 3', name: 'documents 3', weight: 6.941, symbol: 'Li'},
  {position: 'Sys  4', name: 'documents 4', weight: 9.0122, symbol: 'Be'},
  {position:  'Sys 5', name: 'documents 5', weight: 10.811, symbol: 'B'},
  {position:  'Sys 6', name: 'documents 6', weight: 12.0107, symbol: 'C'},
  {position:  'Sys 7', name: 'documents 7', weight: 14.0067, symbol: 'N'},
  {position:  'Sys 8', name: 'documents 8', weight: 15.9994, symbol: 'O'},
  {position:  'Sys 9', name: 'documents 9', weight: 18.9984, symbol: 'F'},
  {position:  'Sys 10', name: 'documents 10', weight: 20.1797, symbol: 'Ne'},
  {position:  'Sys 1', name: 'documents 1', weight: 1.0079, symbol: 'H'},
  {position:  'Sys 2', name: 'documents 2', weight: 4.0026, symbol: 'He'},
  {position:  'Sys 3', name: 'documents 3', weight: 6.941, symbol: 'Li'},
  {position: 'Sys  4', name: 'documents 4', weight: 9.0122, symbol: 'Be'},
  {position:  'Sys 5', name: 'documents 5', weight: 10.811, symbol: 'B'},
  {position:  'Sys 6', name: 'documents 6', weight: 12.0107, symbol: 'C'},
  {position:  'Sys 7', name: 'documents 7', weight: 14.0067, symbol: 'N'},
  {position:  'Sys 8', name: 'documents 8', weight: 15.9994, symbol: 'O'},
  {position:  'Sys 9', name: 'documents 9', weight: 18.9984, symbol: 'F'},
  {position:  'Sys 10', name: 'documents 10', weight: 20.1797, symbol: 'Ne'},
  {position:  'Sys 1', name: 'documents 1', weight: 1.0079, symbol: 'H'},
  {position:  'Sys 2', name: 'documents 2', weight: 4.0026, symbol: 'He'},
  {position:  'Sys 3', name: 'documents 3', weight: 6.941, symbol: 'Li'},
  {position: 'Sys  4', name: 'documents 4', weight: 9.0122, symbol: 'Be'},
  {position:  'Sys 5', name: 'documents 5', weight: 10.811, symbol: 'B'},
  {position:  'Sys 6', name: 'documents 6', weight: 12.0107, symbol: 'C'},
  {position:  'Sys 7', name: 'documents 7', weight: 14.0067, symbol: 'N'},
  {position:  'Sys 8', name: 'documents 8', weight: 15.9994, symbol: 'O'},
  {position:  'Sys 9', name: 'documents 9', weight: 18.9984, symbol: 'F'},
  {position:  'Sys 10', name: 'documents 10', weight: 20.1797, symbol: 'Ne'},
];


@Component({
  selector: 'app-documents-table',
  templateUrl: './documents-table.component.html',
  styleUrls: ['./documents-table.component.css']
})
export class DocumentsTableComponent implements OnInit,AfterViewInit {
  
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator;
  @ViewChild(MatSort,{static:true}) sort: MatSort;


  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol','action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  resultsLength = ELEMENT_DATA.length;
  constructor() {  }
   
  ngOnInit(): void {

  }

  ngAfterViewInit() {
    
      this.dataSource.paginator = this.paginator;
      console.log(this.paginator);
      this.dataSource.sort = this.sort;
      this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0 );
  }

}
