import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilterScreenComponent } from '../screens/filter-screen/filter-screen.component';
import { LoginComponent } from '../screens/login/login.component';
import { AddDocumentComponent} from '../screens/add-document/add-document.component'


const routes: Routes = [
  { path:'dashboard',  component:FilterScreenComponent},
  {path:'login', component:LoginComponent},
  {path:"", component:LoginComponent},
  {path:"add-document", component:AddDocumentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
