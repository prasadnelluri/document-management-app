import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {
  public showAdvancedFilter = false;
  fileToUpload: File = null;
  fileName:any=null;
  constructor() { }

  ngOnInit(): void {
  }

  handleFileInput(files: FileList) {
  this.fileToUpload = files.item(0);
  return this.fileName = this.fileToUpload.name;
}

}
