import { Component } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'document-management';
  isNotloginPage = window.location.pathname !='/' &&  !window.location.pathname.includes('/login');

  constructor(private router: Router) {
    router.events.subscribe((routerEvent: Event) => {
        this.isNotloginPage = window.location.pathname !='/' &&  !window.location.pathname.includes('/login');

    });
}

}
